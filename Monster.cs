﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace DungeonRPG.Mechanics
{
    public class Monster
    {
        public static string Name { get; set; }
        public static int Level { get; set; }

        public static int Stamina { get; set; }
        public static int Health { get; set; }
        public static int Strengh { get; set; }
        public static int Agility { get; set; }
        public static int Luck { get; set; }

        public static int XPReward { get; set; }
        public static int GoldReward { get; set; }
        public static string MonsterImg { get; set; }
        public static bool Alive { get; set; }

        protected static Random r = new Random();

        protected static string dungeonPath = ".\\Resources\\Monsters\\";

        protected static List<string> monsterList = new List<string>();
        protected static List<string> bossList = new List<string>();
        protected static List<string> dungeonList = new List<string>();

        /// <summary>
        /// Called when the player hit the "explore" button, generate random monster in the monster list generated
        /// by the dungeon generation method.
        /// </summary>
        public Monster()
        {
            Name = RandNameMonster();
            Level = r.Next(Player.Level -2, Player.Level + 2);
            if (Level < 1)
                Level = 1;
            Stamina = Level * r.Next(8,11);
            Health = Stamina * 10;
            Strengh = Level * r.Next(10,21);
            Agility = Level * r.Next(2, 7);

            Luck = Level;

            XPReward = Level * 10;
            GoldReward = Level * 5;
            MonsterImg = dungeonPath + Player.Position + "\\" + Name + ".png";
            Alive = true;
        }

        /// <summary>
        /// This needs to be loaded when game start to generate dungeon listing.
        /// For futur implementation of a dropdown list where the player can choose where he want to go.
        /// </summary>
        public static void GenDungeonList()
        {
            DirectoryInfo dungeonScan = new DirectoryInfo(dungeonPath);
            DirectoryInfo[] dungeonListing = dungeonScan.GetDirectories();

            foreach (DirectoryInfo dName in dungeonListing)
            {
                int dungeonID = r.Next(dungeonListing.Length);
                string dungeonName = dungeonListing[dungeonID].ToString();
                dungeonList.Add(dungeonName);
            }
        }

        /// <summary>
        /// Alternativly to the GenDungeonList, this method is called when the player want to explore random dungeon.
        /// Need to rewrite the code to better implementation with the GenDungeonList method.
        /// </summary>
        public static void GenDungeon()
        {
            DirectoryInfo dungeonScan = new DirectoryInfo(dungeonPath);
            DirectoryInfo[] dungeonListing = dungeonScan.GetDirectories();

            foreach (DirectoryInfo dName in dungeonListing)
            {
                int dungeonID = r.Next(dungeonListing.Length);
                string dungeonName = dungeonListing[dungeonID].ToString();
                Player.Position = dungeonName;
                monsterList.Clear();
                bossList.Clear();
                GenMonster(dungeonName);
                break;
            }
        }

        /// <summary>
        /// Called by the GenDungeon method, or by the futur dropdown list when the player choose to explore
        /// a specific dungeon.
        /// </summary>
        /// <param name="dungeonname"></param>
        public static void GenMonster(string dungeonname)
        {
            DirectoryInfo di = new DirectoryInfo(dungeonPath + Player.Position);
            FileInfo[] fi = di.GetFiles("*.png");

            foreach (FileInfo monsterImg in fi)
            {
                if (!monsterImg.Name.Contains("Boss - "))
                    monsterList.Add(Path.GetFileNameWithoutExtension(monsterImg.Name));
                else
                    bossList.Add(Path.GetFileNameWithoutExtension(monsterImg.Name));
            }
        }

        private static string RandNameMonster()
        {
            int search = r.Next(monsterList.Count);
            string name = monsterList[search].ToString();
            return name;
        }
    }

    /// <summary>
    /// Need to review this part
    /// </summary>
    public class Boss : Monster
    {
        public Boss()
        {
            Name = RandNameBoss();
            Level = r.Next(Player.Level + 2, Player.Level + 5);
            Health = Level * r.Next(50, 55);
            Strengh = Level * r.Next(20, 25);
            XPReward = Level * 20;
            GoldReward = Level * 100;
            MonsterImg = dungeonPath + Player.Position + "\\" + Name + ".png";
            Alive = true;
            MessageBox.Show("DEBUG: NEW BOSS SPAWNED !");
        }

        private static string RandNameBoss()
        {
            int search = r.Next(bossList.Count);
            string name = bossList[search].ToString();
            return name;
        }
    }
}
